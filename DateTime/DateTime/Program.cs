﻿try
{

    DateTimeExercise dateTimeExercise = new DateTimeExercise();

    Console.WriteLine("=== Time Management Exercise ===");
    Console.WriteLine("1. Calculate days between two dates");
    Console.WriteLine("2. Specify day of the week");
    Console.WriteLine("3. Calculate age and zodiac sign");
    Console.WriteLine("0. Exit\n");

    bool exit = false;

    while (!exit)
    {
        Console.Write("Enter your choice: ");
        int choice = Convert.ToInt32(Console.ReadLine());

        switch (choice)
        {
            case 1:
                dateTimeExercise.CalculateDaysBetweenTwoDates();
                break;
            case 2:
                dateTimeExercise.SpecifyDayOfTheWeek();
                break;
            case 3:
                dateTimeExercise.CalculateAgeAndZodiac();
                break;
            case 0:
                exit = true;
                break;
            default:
                Console.WriteLine("Invalid choice. Please try again.");
                break;
        }
        Console.WriteLine();

    }
}
catch (Exception e)
{

    Console.WriteLine(e.Message);
}
class DateTimeExercise
{
    public void CalculateDaysBetweenTwoDates()
    {
        Console.WriteLine("Enter the first date (dd/mm/yyyy):");
        DateTime firstDate = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);
        Console.WriteLine("Enter the second date (dd/mm/yyyy):");
        DateTime secondDate = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);
        TimeSpan duration = secondDate - firstDate;
        int days = duration.Days;
        Console.WriteLine($"Number of days between the two dates:{days}");
    }
    public void SpecifyDayOfTheWeek()
    {
        Console.WriteLine("Enter a date (dd/mm/yyyy):");
        DateTime date = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);
        string dayOfWeek = date.ToString("dddd");
        Console.WriteLine($"That day is a {dayOfWeek}");
    }
    public void CalculateAgeAndZodiac()
    {
        Console.WriteLine("Enter your date of birth (dd/mm/yyyy):");
        DateTime birthDate = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);

        DateTime today = DateTime.Today;
        int age = today.Year - birthDate.Year;

        // Check if the birth date hasn't occurred yet this year
        if (birthDate > today.AddYears(-age))
            age--;

        Console.WriteLine($"Your age is {age}");

        string zodiacSign = GetZodiacSign(birthDate);
        Console.WriteLine($"Your zodiac sign is {zodiacSign}");

    }
    private string GetZodiacSign(DateTime date)
    {
        int month = date.Month;
        int day = date.Day;
        string zodiacSign=string.Empty;

        if ((month == 3 && day >= 21) || (month == 4 && day <= 19))
            zodiacSign = "Aries";
        else if ((month == 4 && day >= 20) || (month == 5 && day <= 20))
            zodiacSign = "Taurus";
        else if ((month == 5 && day >= 21) || (month == 6 && day <= 20))
            zodiacSign = "Gemini";
        else if ((month == 6 && day >= 21) || (month == 7 && day <= 22))
            zodiacSign = "Cancer";
        else if ((month == 7 && day >= 23) || (month == 8 && day <= 22))
            zodiacSign = "Leo";
        else if ((month == 8 && day >= 23) || (month == 9 && day <= 22))
            zodiacSign = "Virgo";
        else if ((month == 9 && day >= 23) || (month == 10 && day <= 22))
            zodiacSign = "Libra";
        else if ((month == 10 && day >= 23) || (month == 11 && day <= 21))
            zodiacSign = "Scorpio";
        else if ((month == 11 && day >= 22) || (month == 12 && day <= 21))
            zodiacSign = "Sagittarius";
        else if ((month == 12 && day >= 22) || (month == 1 && day <= 19))
            zodiacSign = "Capricorn";
        else if ((month == 1 && day >= 20) || (month == 2 && day <= 18))
            zodiacSign = "Aquarius";
        else if ((month == 2 && day >= 19) || (month == 3 && day <= 20))
            zodiacSign = "Pisces";

        return zodiacSign;

    }
}


