﻿MyStack<int> intStack = new MyStack<int>(5);
intStack.Push(1);
intStack.Push(2);
Console.WriteLine(intStack.Pop());  // Output: 2

MyStack<string> stringStack = new MyStack<string>(3);
stringStack.Push("Hello");
stringStack.Push("World");
Console.WriteLine(stringStack.Peek());  // Output: World

MyStack<Person> personStack = new MyStack<Person>(2);
personStack.Push(new Person("John", "12345"));
Console.WriteLine(personStack.Peek().Name);  // Output: John

