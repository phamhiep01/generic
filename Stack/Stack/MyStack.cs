﻿

class MyStack<T>
{
    private int maxElements;
    private int lastItemIndex;
    private T[] items;
    public int Count { get; private set; }
    public bool IsEmpty => Count == 0;

    public MyStack(int num)
    {
        maxElements = num;
        lastItemIndex = -1;
        items = new T[maxElements];

    }
    public void Push(T item)
    {
        if (Count == maxElements)
        {
            throw new StackOverflowException("Stack has reached its size limit");
        }
        lastItemIndex++;
        items[lastItemIndex] = item;
        Count++;
    }
    public T Pop()
    {
        if(IsEmpty)
        {
            throw new InvalidOperationException("Stack is empty");
        }
        T item= items[lastItemIndex];
        items[lastItemIndex]=default(T);
        lastItemIndex--;
        Count--;
        return item;
    }
    public T Peek()
    {
        if(IsEmpty)
        { throw new InvalidOperationException("Stack is empty"); }
        return items[lastItemIndex];
    }
    public void Clear()
    {
        Array.Clear(items, 0, Count);
        lastItemIndex = -1;
        Count = 0;
    }
}

