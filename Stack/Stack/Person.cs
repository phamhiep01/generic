﻿



class Person
{
    public string Name { get; set; }
    public string PhoneNum { get; set; }

    public Person(string name, string phoneNum)
    {
        Name = name;
        PhoneNum = phoneNum;
    }
}

