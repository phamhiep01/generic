﻿
class Person
{
    public string Name { get; set; }
    public int PhoneNumber { get; set; }

    public Person(string name, int phoneNumber)
    {
        Name = name;
        PhoneNumber = phoneNumber;
    }
}
