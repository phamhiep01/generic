﻿
class MyQueue<T>
{
    private int maxSize;
    private T[] items;
    private int firstItemIndex;
    private int count;

    public bool isFull => count == maxSize;
    public bool isEmpty => count == 0;
    public MyQueue(int num)
    {
        maxSize = num;
        items = new T[num];
        firstItemIndex = 0;
        count = 0;
    }

    public void Enqueue(T item)
    {
        if (isFull)
        {
            throw new InvalidOperationException("Queue is full");
        }

        int index = (firstItemIndex + count) % maxSize;
        items[index] = item;
        count++;
    }
    public T Dequeue()
    {
        if (isEmpty)
        {
            throw new InvalidOperationException("Queue is empty");
        }
        T item = items[firstItemIndex];
        items[firstItemIndex] = default(T);
        firstItemIndex = (firstItemIndex + 1) % maxSize;
        count--;
        return item;
    }
    public T Peek()
    {
        if (isEmpty)
        {
            throw new InvalidOperationException("Queue is empty");
        }
        return items[firstItemIndex];
    }
    public void Clear()
    {
        Array.Clear(items, 0, count);
        firstItemIndex = 0;
        count = 0;
    }
    public int Count
    {
        get { return count; }
    }
    public bool IsEmpty
    {
        get { return count == 0; }
    }

}
