﻿
try
{
    MyQueue<int> intQueue = new MyQueue<int>(5);
    MyQueue<string> stringQueue = new MyQueue<string>(5);
    MyQueue<Person> personQueue = new MyQueue<Person>(5);
    MyQueue<Student> studentQueue = new MyQueue<Student>(5);

    int choice = 0;
    do
    {
        Console.WriteLine("----- Menu -----");
        Console.WriteLine("1. Enqueue");
        Console.WriteLine("2. Dequeue");
        Console.WriteLine("3. Peek");
        Console.WriteLine("4. IsEmpty");
        Console.WriteLine("5. Clear");
        Console.WriteLine("6. Exit");
        Console.Write("Enter your choice: ");
        choice = int.Parse(Console.ReadLine());

        Console.WriteLine("----- Menu -----");
        Console.WriteLine("1. Enqueue");
        Console.WriteLine("2. Dequeue");
        Console.WriteLine("3. Peek");
        Console.WriteLine("4. IsEmpty");
        Console.WriteLine("5. Clear");
        Console.WriteLine("6. Exit");
        Console.Write("Enter your choice: ");
        choice = int.Parse(Console.ReadLine());

        switch (choice)
        {
            case 1:
                Console.Write("Enter an item: ");
                string item = Console.ReadLine();
                stringQueue.Enqueue(item);
                Console.WriteLine("Enqueued successfully.");
                break;
            case 2:
                string dequeuedItem = stringQueue.Dequeue();
                Console.WriteLine("Dequeued item: " + dequeuedItem);
                break;
            case 3:

                string peekedItem = stringQueue.Peek();
                Console.WriteLine("Peeked item: " + peekedItem);
                break;
            case 4:
                Console.WriteLine("IsEmpty: " + stringQueue.IsEmpty);
                break;
            case 5:
                stringQueue.Clear();
                Console.WriteLine("Queue cleared.");
                break;
            case 6:
                Console.WriteLine("Exiting...");
                break;
            default:
                Console.WriteLine("Invalid choice. Please try again.");
                break;
        }
        Console.WriteLine();
    } while (choice != 6);
}


catch (Exception e)
{

    Console.WriteLine(e.Message);
}
