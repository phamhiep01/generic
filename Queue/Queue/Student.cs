﻿
struct Student
{
    public int ID { get; set; }
    public string Name { get; set; }

    public Student(int id, string name)
    {
        ID = id;
        Name = name;
    }
}